﻿namespace DiceGame
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNumPlaysText = new System.Windows.Forms.Label();
            this.lblNumWinsText = new System.Windows.Forms.Label();
            this.lblNumLossesText = new System.Windows.Forms.Label();
            this.lblUserGuess = new System.Windows.Forms.Label();
            this.lblNumPlaysResult = new System.Windows.Forms.Label();
            this.lblNumWinsResult = new System.Windows.Forms.Label();
            this.lblNumLossesResult = new System.Windows.Forms.Label();
            this.rTxtStatsBox = new System.Windows.Forms.RichTextBox();
            this.txtBoxGuess = new System.Windows.Forms.TextBox();
            this.picBoxDice = new System.Windows.Forms.PictureBox();
            this.btnRoll = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.grpBoxGameInfo = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxDice)).BeginInit();
            this.grpBoxGameInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblNumPlaysText
            // 
            this.lblNumPlaysText.AutoSize = true;
            this.lblNumPlaysText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumPlaysText.Location = new System.Drawing.Point(6, 25);
            this.lblNumPlaysText.Name = "lblNumPlaysText";
            this.lblNumPlaysText.Size = new System.Drawing.Size(148, 13);
            this.lblNumPlaysText.TabIndex = 0;
            this.lblNumPlaysText.Text = "Number of Times Played:";
            // 
            // lblNumWinsText
            // 
            this.lblNumWinsText.AutoSize = true;
            this.lblNumWinsText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumWinsText.Location = new System.Drawing.Point(6, 59);
            this.lblNumWinsText.Name = "lblNumWinsText";
            this.lblNumWinsText.Size = new System.Drawing.Size(136, 13);
            this.lblNumWinsText.TabIndex = 1;
            this.lblNumWinsText.Text = "Number of Times Won:";
            // 
            // lblNumLossesText
            // 
            this.lblNumLossesText.AutoSize = true;
            this.lblNumLossesText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumLossesText.Location = new System.Drawing.Point(6, 93);
            this.lblNumLossesText.Name = "lblNumLossesText";
            this.lblNumLossesText.Size = new System.Drawing.Size(134, 13);
            this.lblNumLossesText.TabIndex = 2;
            this.lblNumLossesText.Text = "Number of Times Lost:";
            // 
            // lblUserGuess
            // 
            this.lblUserGuess.AutoSize = true;
            this.lblUserGuess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserGuess.Location = new System.Drawing.Point(46, 178);
            this.lblUserGuess.Name = "lblUserGuess";
            this.lblUserGuess.Size = new System.Drawing.Size(140, 13);
            this.lblUserGuess.TabIndex = 3;
            this.lblUserGuess.Text = "Enter Your Guess (1-6):";
            // 
            // lblNumPlaysResult
            // 
            this.lblNumPlaysResult.AutoSize = true;
            this.lblNumPlaysResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumPlaysResult.Location = new System.Drawing.Point(177, 25);
            this.lblNumPlaysResult.Name = "lblNumPlaysResult";
            this.lblNumPlaysResult.Size = new System.Drawing.Size(0, 13);
            this.lblNumPlaysResult.TabIndex = 4;
            // 
            // lblNumWinsResult
            // 
            this.lblNumWinsResult.AutoSize = true;
            this.lblNumWinsResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumWinsResult.Location = new System.Drawing.Point(177, 59);
            this.lblNumWinsResult.Name = "lblNumWinsResult";
            this.lblNumWinsResult.Size = new System.Drawing.Size(0, 13);
            this.lblNumWinsResult.TabIndex = 5;
            // 
            // lblNumLossesResult
            // 
            this.lblNumLossesResult.AutoSize = true;
            this.lblNumLossesResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumLossesResult.Location = new System.Drawing.Point(177, 93);
            this.lblNumLossesResult.Name = "lblNumLossesResult";
            this.lblNumLossesResult.Size = new System.Drawing.Size(0, 13);
            this.lblNumLossesResult.TabIndex = 6;
            // 
            // rTxtStatsBox
            // 
            this.rTxtStatsBox.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rTxtStatsBox.Location = new System.Drawing.Point(12, 326);
            this.rTxtStatsBox.Name = "rTxtStatsBox";
            this.rTxtStatsBox.ReadOnly = true;
            this.rTxtStatsBox.Size = new System.Drawing.Size(481, 157);
            this.rTxtStatsBox.TabIndex = 7;
            this.rTxtStatsBox.Text = "";
            // 
            // txtBoxGuess
            // 
            this.txtBoxGuess.Location = new System.Drawing.Point(192, 175);
            this.txtBoxGuess.MaxLength = 1;
            this.txtBoxGuess.Name = "txtBoxGuess";
            this.txtBoxGuess.Size = new System.Drawing.Size(32, 20);
            this.txtBoxGuess.TabIndex = 9;
            // 
            // picBoxDice
            // 
            this.picBoxDice.Location = new System.Drawing.Point(49, 214);
            this.picBoxDice.Name = "picBoxDice";
            this.picBoxDice.Size = new System.Drawing.Size(100, 94);
            this.picBoxDice.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxDice.TabIndex = 10;
            this.picBoxDice.TabStop = false;
            // 
            // btnRoll
            // 
            this.btnRoll.Location = new System.Drawing.Point(192, 214);
            this.btnRoll.Name = "btnRoll";
            this.btnRoll.Size = new System.Drawing.Size(75, 23);
            this.btnRoll.TabIndex = 11;
            this.btnRoll.Text = "Roll";
            this.btnRoll.UseVisualStyleBackColor = true;
            this.btnRoll.Click += new System.EventHandler(this.BtnChangeImage_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(192, 258);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 12;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // grpBoxGameInfo
            // 
            this.grpBoxGameInfo.BackColor = System.Drawing.Color.Wheat;
            this.grpBoxGameInfo.Controls.Add(this.lblNumPlaysText);
            this.grpBoxGameInfo.Controls.Add(this.lblNumWinsText);
            this.grpBoxGameInfo.Controls.Add(this.lblNumLossesText);
            this.grpBoxGameInfo.Controls.Add(this.lblNumPlaysResult);
            this.grpBoxGameInfo.Controls.Add(this.lblNumWinsResult);
            this.grpBoxGameInfo.Controls.Add(this.lblNumLossesResult);
            this.grpBoxGameInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpBoxGameInfo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grpBoxGameInfo.Location = new System.Drawing.Point(37, 24);
            this.grpBoxGameInfo.Name = "grpBoxGameInfo";
            this.grpBoxGameInfo.Size = new System.Drawing.Size(237, 130);
            this.grpBoxGameInfo.TabIndex = 13;
            this.grpBoxGameInfo.TabStop = false;
            this.grpBoxGameInfo.Text = "Game Info";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(505, 508);
            this.Controls.Add(this.grpBoxGameInfo);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnRoll);
            this.Controls.Add(this.picBoxDice);
            this.Controls.Add(this.txtBoxGuess);
            this.Controls.Add(this.rTxtStatsBox);
            this.Controls.Add(this.lblUserGuess);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Form1";
            this.Text = "Dice Guessing Game";
            ((System.ComponentModel.ISupportInitialize)(this.picBoxDice)).EndInit();
            this.grpBoxGameInfo.ResumeLayout(false);
            this.grpBoxGameInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNumPlaysText;
        private System.Windows.Forms.Label lblNumWinsText;
        private System.Windows.Forms.Label lblNumLossesText;
        private System.Windows.Forms.Label lblUserGuess;
        private System.Windows.Forms.Label lblNumPlaysResult;
        private System.Windows.Forms.Label lblNumWinsResult;
        private System.Windows.Forms.Label lblNumLossesResult;
        private System.Windows.Forms.RichTextBox rTxtStatsBox;
        private System.Windows.Forms.TextBox txtBoxGuess;
        private System.Windows.Forms.PictureBox picBoxDice;
        private System.Windows.Forms.Button btnRoll;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.GroupBox grpBoxGameInfo;
    }
}

